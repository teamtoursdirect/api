<?php

namespace TTD\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Database\DatabaseManager;
use Illuminate\Contracts\Hashing\Hasher;
use TTD\Api\Headers;

/**
 * Class Authenticate
 *
 * @package App\Http\Middleware
 */
class Authenticate
{
    /**
     * @var \Illuminate\Database\DatabaseManager
     */
    protected $db;

    /**
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;

    /**
     * @var \TTD\Api\Headers
     */
    protected $headers;

    /**
     * Authenticate constructor.
     *
     * @param \Illuminate\Contracts\Hashing\Hasher $hasher
     * @param \Illuminate\Database\DatabaseManager $db
     * @param \TTD\Api\Headers                     $headers
     */
    public function __construct(Hasher $hasher, DatabaseManager $db, Headers $headers)
    {
        $this->db = $db;
        $this->hasher = $hasher;
        $this->headers = $headers;
    }

    /**
     * @param \Illuminate\Contracts\Hashing\Hasher $hasher
     * @return $this
     */
    public function setHasher(Hasher $hasher)
    {
        $this->hasher = $hasher;
        return $this;
    }

    /**
     * @param \Illuminate\Database\DatabaseManager $db
     * @return $this
     */
    public function setDatabaseManager(DatabaseManager $db)
    {
        $this->db = $db;
        return $this;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle($request, Closure $next)
    {
        if (($key = $this->headers->getKey($request)) && ($secret = $this->headers->getSecret($request))) {
            if ($this->validCredentials($key, $secret)) {
                return $next($request);
            }
        }
        return response()->json(['error' => 'Unauthenticated'], 401);
    }

    /**
     * Are the credentials valid?
     *
     * @param string $key
     * @param string $secret
     * @return bool
     */
    public function validCredentials(string $key, string $secret)
    {
        $user = $this->getConnection()->table('api_users')->where('key', '=', $key)->first();
        if ($user && $this->hasher->check($secret, $user->secret)) {
            return true;
        }
        return false;
    }

    /**
     * @return \Illuminate\Database\Connection
     */
    protected function getConnection()
    {
        return $this->db->connection($this->getConnectionName());
    }

    /**
     * @return string
     */
    protected function getConnectionName(): string
    {
        return config('api.connection_name');
    }
}
