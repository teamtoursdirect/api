<?php

namespace TTD\Api;

use Illuminate\Support\ServiceProvider;
use TTD\Api\Console\Commands\CreateUser;

class ApiServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom($this->getBasePath('config/api.php'), 'api');
        $this->loadMigrationsFrom($this->getBasePath('migrations'));

        if ($this->app->runningInConsole()) {
            $this->commands([CreateUser::class]);
        }
    }

    /**
     * Get the base path of this package
     *
     * @param string $path
     * @return string
     */
    protected function getBasePath($path = '')
    {
        return rtrim(dirname(__FILE__), '/') . '/' . ltrim($path, '/');
    }
}
