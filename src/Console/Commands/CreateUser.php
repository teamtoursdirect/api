<?php

namespace TTD\Api\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Database\DatabaseManager;

/**
 * Class CreateUser
 *
 * @package App\Console\Commands
 */
class CreateUser extends Command
{
    /**
     * @var string
     */
    protected $signature = 'api:create-user';

    /**
     * @var string
     */
    protected $description = 'Create a new user for this API.';


    public function handle(DatabaseManager $db, Hasher $hasher, string $key = null)
    {
        if (!$key) {
            $key = $this->ask('What is the users key?');
        }

        // make sure that they typed the secret correctly...
        $secret = $this->secret('What is the users secret? (it will look like nothing is entering, but it is)');
        $confirm = $this->secret('Please confirm the users secret');

        if ($secret != $confirm) {
            $this->error('The secret did not match. Please try again.');
            return $this->handle($db, $hasher, $key);
        }

        $insert = $db->connection($this->getConnectionName())->table('api_users')->insertGetId([
            'key' => $key,
            'secret' => $hasher->make($secret)
        ]);

        $this->info('The user has been created.');
        $this->info("ID: {$insert}");
        $this->info("Key: {$key}");
    }

    /**
     * @return string
     */
    public function getConnectionName(): string
    {
        return config('api.connection_name');
    }
}
