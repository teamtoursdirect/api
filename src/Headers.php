<?php

namespace TTD\Api;

use Illuminate\Http\Request;

/**
 * Class Headers
 *
 * @package TTD\Api
 */
class Headers
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function getHeaders(Request $request)
    {
        return $this->parseHeaders(collect($request->headers)->toArray());
    }

    /**
     * @param array $headers
     * @return array
     */
    public function parseHeaders(array $headers)
    {
        $newHeaders = [];
        collect($headers)->each(function ($value, $key) use (&$newHeaders) {
            $newHeaders[strtolower($key)] = $value;
        });
        return $newHeaders;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed|null
     */
    public function getSecret(Request $request)
    {
        return $this->getHeaders($request)['api-secret'][0] ?? null;
    }

    /**
     * Get the api key from the headers
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed|null
     */
    public function getKey(Request $request)
    {
        return $this->getHeaders($request)['api-key'][0] ?? null;
    }
}
