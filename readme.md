# API package

A base API package for all TTD APIs to use.

## Installation

**Add the repository to composer.json**

Add the following to your composer.json (replace xxxx with your BitBucket username).
We could change the URL if we made this package public.

    "repositories": [
            {
                "type": "vcs",
                "url": "xxxx@bitbucket.org:teamtoursdirect/api.git"
            }
        ]
        

**Register the service provider:**

Lumen: `$app->register(TTD\Api\ApiServiceProvider:class);`

**Register the middleware**

Register the middleware globally:
    
    Edit app/bootstrap.php and add
    $app->middleware([
        TTD\Api\Http\Middleware\Authenticate::class
    ]);
    
Or give it an alias and wrap your routes in it...

    Edit app/bootstrap.php and add
    $app->routeMiddleware([
        'auth' => App\Http\Middleware\Authenticate::class,
    ]);


## Commands available

* php artisan api:create-user - create a user through the CLI

## Authentication

This package uses aa key and secret for authentication that is posted in the headers, with the parent key being 
"Api-Key" and "Api-Secret" (not case sensitive).

    Example:
    GET /keys
    Accept: application/json
    Api-Key: some-super-long-string
    Api-Secret: some-even-longer-string