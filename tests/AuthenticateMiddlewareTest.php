<?php

class AuthenticateMiddlewareTest extends \Tests\TestCase
{
    /**
     * @var \TTD\Api\Http\Middleware\Authenticate
     */
    protected $middleware;

    /**
     * @var \Illuminate\Database\DatabaseManager|\Mockery\Mock
     */
    protected $db;

    /**
     * @var \Illuminate\Contracts\Hashing\Hasher|\Mockery\Mock
     */
    protected $hasher;

    public function setUp()
    {
        parent::setUp();
        $this->db = Mockery::mock('Illuminate\Database\DatabaseManager');
        $this->hasher = Mockery::mock('Illuminate\Contracts\Hashing\Hasher');
        $this->middleware = app('TTD\Api\Http\Middleware\Authenticate');
        $this->middleware->setDatabaseManager($this->db)
            ->setHasher($this->hasher);
    }

    public function test_valid_credentials_bad_key()
    {
        $this->db->shouldReceive('connection')->once()->andReturn($this->db);
        $this->db->shouldReceive('table')->once()->with('api_users')->andReturn($this->db);
        $this->db->shouldReceive('where')->once()->with('key', '=', 'a')->andReturn($this->db);
        $this->db->shouldReceive('first')->once()->andReturn(null);

        $this->assertFalse($this->middleware->validCredentials('a', 'b'));
    }

    public function test_valid_credentials_invalid_secret()
    {
        $this->db->shouldReceive('connection')->once()->andReturn($this->db);
        $this->db->shouldReceive('table')->once()->with('api_users')->andReturn($this->db);
        $this->db->shouldReceive('where')->once()->with('key', '=', 'a')->andReturn($this->db);
        $this->db->shouldReceive('first')->once()->andReturn((object)[
            'secret' => 'xxx'
        ]);

        $this->hasher->shouldReceive('check')->once()->with('b', 'xxx')->andReturn(false);

        $this->assertFalse($this->middleware->validCredentials('a', 'b'));
    }

    public function test_valid_credentials_valid()
    {
        $this->db->shouldReceive('connection')->once()->andReturn($this->db);
        $this->db->shouldReceive('table')->once()->with('api_users')->andReturn($this->db);
        $this->db->shouldReceive('where')->once()->with('key', '=', 'a')->andReturn($this->db);
        $this->db->shouldReceive('first')->once()->andReturn((object)[
            'secret' => 'xxx'
        ]);

        $this->hasher->shouldReceive('check')->once()->with('b', 'xxx')->andReturn(true);

        $this->assertTrue($this->middleware->validCredentials('a', 'b'));
    }

    public function test_handle_no_headers()
    {
        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = [
        ];
        $response = $this->middleware->handle($request, function () {
            return true;
        });
        $this->assertEquals('Unauthenticated', $response->getData()->error);
    }

    public function test_handle_only_key()
    {
        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = [
            'api-Key' => ['11'],
            //'api-secret' => ['22'],
        ];
        $response = $this->middleware->handle($request, function () {
            return true;
        });
        $this->assertEquals('Unauthenticated', $response->getData()->error);
    }

    public function test_handle_only_secret()
    {
        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = [
            //'api-Key' => ['11'],
            'api-secret' => ['22'],
        ];
        $response = $this->middleware->handle($request, function () {
            return true;
        });
        $this->assertEquals('Unauthenticated', $response->getData()->error);
    }

    public function test_handle_bad_key_and_secret()
    {
        $mock = Mockery::mock('TTD\Api\Http\Middleware\Authenticate[validCredentials]', [$this->hasher, $this->db, app(\TTD\Api\Headers::class)]);
        $mock->shouldReceive('validCredentials')->once()->andReturn(false);

        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = [
            'api-Key' => ['11'],
            'api-secret' => ['22'],
        ];
        $response = $mock->handle($request, function () {
            return true;
        });
        $this->assertEquals('Unauthenticated', $response->getData()->error);
    }

    public function test_handle()
    {
        $mock = Mockery::mock('TTD\Api\Http\Middleware\Authenticate[validCredentials]', [$this->hasher, $this->db, app(\TTD\Api\Headers::class)]);
        $mock->shouldReceive('validCredentials')->once()->andReturn(true);

        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = [
            'api-Key' => ['aa'],
            'api-secret' => ['bbb'],
        ];
        $response = $mock->handle($request, function () {
            return true;
        });

        $this->assertEquals(true, $response);
    }
}
