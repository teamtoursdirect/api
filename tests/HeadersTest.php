<?php

class HeadersTest extends \Tests\TestCase
{
    /**
     * @var \TTD\Api\Headers
     */
    protected $headers;

    public function setUp()
    {
        parent::setUp();
        $this->headers = app('TTD\Api\Headers');
    }

    public function test_get_headers()
    {
        $headers = [
            'Some-KeY' => 'xxXxx'
        ];

        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = $headers;

        $getHeaders = $this->headers->getHeaders($request);

        $this->assertTrue(isset($getHeaders['some-key']), 'Missing key "some-key"');
        $this->assertEquals('xxXxx', $getHeaders['some-key']);
    }

    public function test_get_key_no_key()
    {
        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = ['a' => 'b'];

        $this->assertNull($this->headers->getKey($request));
    }

    public function test_get_key()
    {
        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = ['apI-Key' => 'b'];

        $this->assertEquals('b', $this->headers->getKey($request));
    }

    public function test_get_secret_no_secret()
    {
        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = ['a' => 'b'];

        $this->assertNull($this->headers->getSecret($request));
    }

    public function test_get_secret()
    {
        $request = Mockery::mock('Illuminate\Http\Request');
        $request->headers = ['apI-SEcret' => 'b'];

        $this->assertEquals('b', $this->headers->getSecret($request));
    }
}
