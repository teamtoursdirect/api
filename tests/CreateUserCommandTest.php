<?php

class CreateUserCommandTest extends \Tests\TestCase
{
    public function test_handle()
    {
        config()->set('api.connection_name', 'testtt');
        $mock = $this->buildMock();

        // check that the matching secrets matches...
        $mock->shouldReceive('ask')->once()->andReturn('some_key');
        $mock->shouldReceive('secret')->once()->andReturn('a');
        $mock->shouldReceive('secret')->once()->andReturn('aa');
        $mock->shouldReceive('secret')->once()->andReturn('aaaa');
        $mock->shouldReceive('secret')->once()->andReturn('aaaa');

        $mock->shouldReceive('error')->once();

        /*$user = $this->user();
        $user->shouldReceive('create')->once()->with([
            'key' => 'some_key',
            'secret' => 'aaaa'
        ])->andReturn((object)[
            'id' => 22
        ]);*/
        $hasher = $this->hasher();
        $hasher->shouldReceive('make')->once()->with('aaaa')->andReturn('hashed');

        $db = $this->db();
        $db->shouldReceive('connection')->once()->with('testtt')->andReturn($db);
        $db->shouldReceive('table')->once()->with('api_users')->andReturn($db);
        $db->shouldReceive('insertGetId')->once()->with([
            'key' => 'some_key',
            'secret' => 'hashed'
        ])->andReturn(22);

        $mock->shouldReceive('info')->times(3);

        $mock->handle($db, $hasher);
    }

    /**
     * @param array $mocks
     * @return \TTD\Api\Console\Commands\CreateUser|\Mockery\Mock
     */
    protected function buildMock(array $mocks = ['ask', 'secret', 'info', 'error'])
    {
        return Mockery::mock('TTD\Api\Console\Commands\CreateUser[' . implode(',', $mocks) . ']');
    }

    /**
     * @return \Illuminate\Database\DatabaseManager|\Mockery\Mock
     */
    protected function db()
    {
        return Mockery::mock('Illuminate\Database\DatabaseManager');
    }

    /**
     * @return \Illuminate\Contracts\Hashing\Hasher|\Mockery\Mock
     */
    protected function hasher()
    {
        return Mockery::mock('Illuminate\Contracts\Hashing\Hasher');
    }
}
